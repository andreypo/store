package co.spribe.store.core.repos;

import co.spribe.store.core.model.Order;
import co.spribe.store.core.model.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepo extends CrudRepository<Order, Long> {
    List<Order> findByOrdStatus(Status ordStatus);
}
