package co.spribe.store.core.repos;

import co.spribe.store.core.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepo extends CrudRepository<Product, Long> {
}