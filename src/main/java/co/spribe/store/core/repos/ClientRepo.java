package co.spribe.store.core.repos;

import co.spribe.store.core.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepo extends CrudRepository<Client, Long> {
}
