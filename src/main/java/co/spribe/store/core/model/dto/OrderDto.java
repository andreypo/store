package co.spribe.store.core.model.dto;

import co.spribe.store.core.model.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Data
public class OrderDto {
    @JsonIgnore
    private Long orderId;
    @JsonIgnore
    private Status ordStatus;
    @JsonProperty("productMap")
    private Map<Long, Integer> productMap;
    @JsonProperty("clientId")
    private long clientId;

    public OrderDto(Status ordStatus, Map<Long, Integer> productMap, long clientId) {
        this.ordStatus = ordStatus;
        this.productMap = productMap;
        this.clientId = clientId;
    }
}
