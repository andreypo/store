package co.spribe.store.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Entity
@ToString
@Table(name = "orders")
public class Order extends BaseEntity {

    @Column
    private double totalPrice;

    @Column
    private double totalWeight;

    @Column
    private Status ordStatus;

    @ElementCollection(fetch = FetchType.LAZY)
    private Map<Long, Integer> productMap;
    @Column
    private long clientId;

    public Order(Map<Long, Integer> productMap, long clientId) {
        this.productMap = productMap;
        this.clientId = clientId;
    }
}
