package co.spribe.store.core.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@ToString
@Table(name = "products")
public class Product extends BaseEntity {
    @Column
    private String name;
    @Column
    private double weight;
    @Column
    private double price;
    @Column
    private int amount;

    public Product(String name, double weight, double price) {
        this.name = name;
        this.weight = weight;
        this.price = price;
    }

    public Product(String name, double weight, double price, int amount) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.amount = amount;
    }

    public synchronized void updateAmount(int delta) {
        amount += delta;
    }

    @Override
    public boolean equals(Object o) {
        return Objects.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, weight, price);
    }
}
