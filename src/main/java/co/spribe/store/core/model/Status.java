package co.spribe.store.core.model;

public enum Status {
    NEW, PENDING, COMPLETED, REJECTED, CANCELLED
}
