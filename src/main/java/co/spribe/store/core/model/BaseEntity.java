package co.spribe.store.core.model;

import javax.persistence.*;

@MappedSuperclass
public class BaseEntity {
    @Version
    protected Long version;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
}
