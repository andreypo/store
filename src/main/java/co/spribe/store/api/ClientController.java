package co.spribe.store.api;

import co.spribe.store.core.model.Client;
import co.spribe.store.core.repos.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/store")
public class ClientController {
    @Autowired
    private ClientRepo clientRepo;

    @PostMapping(path = "/client")
    public @ResponseBody
    ResponseEntity<Client> addNewClient(@RequestParam String firstName
            , @RequestParam String lastName) {
        var client = new Client(firstName, lastName);
        clientRepo.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @GetMapping(path = "/client/all")
    public @ResponseBody
    Iterable<Client> getAllClients() {
        return clientRepo.findAll();
    }

    @GetMapping("/client/{id}")
    public Client getOneClientById(@PathVariable Long id) {
        return clientRepo.findById(id)
                .orElseThrow(() -> new NoSuchElementException(id.toString()));
    }
}