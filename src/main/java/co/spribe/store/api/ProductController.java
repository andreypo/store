package co.spribe.store.api;

import co.spribe.store.core.model.Product;
import co.spribe.store.core.repos.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/store")
public class ProductController {
    @Autowired
    private ProductRepo productRepo;

    @PostMapping(path = "/product")
    public @ResponseBody
    ResponseEntity<Product> addNewProduct(@RequestParam String name
            , @RequestParam double weight, double price) {
        var product = new Product(name, weight, price);
        productRepo.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);

    }

    @GetMapping(path = "/product/all")
    public @ResponseBody
    Iterable<Product> getAllProducts() {
        return productRepo.findAll();
    }

    @GetMapping("/product/{id}")
    public Product getOneProductById(@PathVariable Long id) {
        return productRepo.findById(id)
                .orElseThrow(() -> new NoSuchElementException(id.toString()));
    }
}