package co.spribe.store.api;

import co.spribe.store.core.model.Order;
import co.spribe.store.core.model.dto.OrderDto;
import co.spribe.store.core.repos.OrderRepo;
import co.spribe.store.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.NoSuchElementException;

@RestController
@RequestMapping(path = "/store")
public class OrderController {
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private OrderService orderService;

    @PostMapping(path = "/order")
    public @ResponseBody
    ResponseEntity<String> addNewOrder(@RequestBody OrderDto orderDto) {
        var order = new Order();
        order.setClientId(orderDto.getClientId());
        order.setProductMap(orderDto.getProductMap());
        orderService.addOrderToQueue(order);
        return new ResponseEntity<>("Order has been added", HttpStatus.OK);
    }

    @GetMapping(path = "/order/all")
    public @ResponseBody
    Iterable<Order> getAllOrders() {
        return orderRepo.findAll();
    }

    @GetMapping("/order/{id}")
    public Order getOneOrderById(@PathVariable Long id) {
        return orderRepo.findById(id)
                .orElseThrow(() -> new NoSuchElementException(id.toString()));
    }
}