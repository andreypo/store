package co.spribe.store.tasks;

import co.spribe.store.core.model.Order;
import co.spribe.store.core.model.Status;
import co.spribe.store.core.repos.OrderRepo;
import co.spribe.store.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *  Task for order recovery.
 *  Started immediately after Spring boot context init.
 *  Adds not completed (new) {@link Order} from database
 *  to processing queue for {@link OrderProcessingTask}
 */
@Component
@Scope("prototype")
public class RecoveryTask implements Runnable {
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private OrderService orderService;
    private static final Logger LOGGER = LoggerFactory.getLogger(RecoveryTask.class);
    @Override
    public void run() {
        Thread.currentThread().setName("OrdRecoveryThread");
        while(!Thread.currentThread().isInterrupted()) {
            LOGGER.info("Starting Store orders recovery from database");
            List<Order> notProcessedOrders = findNotProcessedOrders();
            if (notProcessedOrders != null && !notProcessedOrders.isEmpty()) {
                LOGGER.info("Found total " + notProcessedOrders.size() + " unprocessed orders");
                LOGGER.info("Adding not processed orders to processing queue...");
                orderService.addOrdersToQueue(notProcessedOrders);
            }
            Thread.currentThread().interrupt();
        }
        LOGGER.info("Recovery finished");
    }

    private List<Order> findNotProcessedOrders() {
        return orderRepo.findByOrdStatus(Status.NEW);
    }
}