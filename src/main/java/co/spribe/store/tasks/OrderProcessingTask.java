package co.spribe.store.tasks;

import co.spribe.store.core.model.Order;
import co.spribe.store.core.model.Status;
import co.spribe.store.core.repos.ClientRepo;
import co.spribe.store.core.repos.OrderRepo;
import co.spribe.store.core.repos.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.BlockingQueue;

/**
 * Main task for order processing.
 * Executing in a separate thread, checks if new {@link Order} can be completed,
 * i.e. store has enough products
 */
@Component
@Scope("prototype")
public class OrderProcessingTask implements Runnable {
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private ClientRepo clientRepo;

    private BlockingQueue<Order> processingQueue;
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderProcessingTask.class);

    public void setProcessingQueue(BlockingQueue<Order> processingQueue) {
        this.processingQueue = processingQueue;
    }

    @Override
    public void run() {
        Thread.currentThread().setName("OrdProcessingThread");
        LOGGER.info("Starting processing thread");
        while (!Thread.currentThread().isInterrupted()) {
            Order orderToProcess = null;
            try {
                orderToProcess = processingQueue.take();
                LOGGER.info("Started processing new order ");
                boolean isValidClient = checkClient(orderToProcess);
                if (!isValidClient) {
                    LOGGER.info("Unknown clientId=" + orderToProcess.getClientId());
                }
                Map<Long, Integer> productMap = orderToProcess.getProductMap();
                var canBePlaced = isValidClient && checkExistingProducts(productMap);
                if (canBePlaced) {
                    var totalPrice = 0d;
                    var totalWeight = 0d;
                    for (Map.Entry<Long, Integer> e : productMap.entrySet()) {
                        long productId = e.getKey();
                        int rqstAmt = e.getValue();
                        var product = productRepo.findById(productId).
                                orElseThrow(() -> new NoSuchElementException("Product not found, id=" + productId));
                        totalPrice += product.getPrice() * rqstAmt;
                        totalWeight += product.getWeight() * rqstAmt;
                        product.updateAmount(-rqstAmt);
                        productRepo.save(product);
                    }
                    orderToProcess.setTotalPrice(totalPrice);
                    orderToProcess.setTotalWeight(totalWeight);
                    orderToProcess.setOrdStatus(Status.COMPLETED);
                    LOGGER.info("Order COMPLETED! " + orderToProcess);
                } else {
                    orderToProcess.setOrdStatus(Status.REJECTED);
                    LOGGER.info("Order REJECTED!" + orderToProcess);
                }
            } catch (NoSuchElementException e) {
                LOGGER.error(e.getMessage());
                LOGGER.info("Order REJECTED!" + orderToProcess);
                if (orderToProcess != null) {
                    orderToProcess.setOrdStatus(Status.REJECTED);
                }
            } catch (InterruptedException e) {
                LOGGER.error("Processing interrupted", e);
                Thread.currentThread().interrupt();
            } finally {
                if (orderToProcess != null) {
                    orderRepo.save(orderToProcess);
                    LOGGER.info("Finished saving order " + orderToProcess);
                }
            }
        }
    }

    private boolean checkClient(Order order) {
        return clientRepo.findById(order.getClientId()).isPresent();
    }

    private boolean checkExistingProducts(Map<Long, Integer> productMap) {
        for (Map.Entry<Long, Integer> e : productMap.entrySet()) {
            long productId = e.getKey();
            int rqstAmt = e.getValue();
            var product = productRepo.findById(productId).
                    orElseThrow(() -> new NoSuchElementException("Product not found, id=" + productId));
            if (product.getAmount() < rqstAmt) {
                LOGGER.info("Not enough product '" + product.getName() +
                        "'\n requested " + rqstAmt + " but only have " + product.getAmount());
                return false;
            }
        }
        return true;
    }
}