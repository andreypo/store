package co.spribe.store.service;

import co.spribe.store.core.model.Client;
import co.spribe.store.core.model.Order;
import co.spribe.store.core.model.Product;
import co.spribe.store.core.repos.ClientRepo;
import co.spribe.store.core.repos.OrderRepo;
import co.spribe.store.core.repos.ProductRepo;
import co.spribe.store.tasks.OrderProcessingTask;
import co.spribe.store.tasks.RecoveryTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.*;

@Service
public class OrderService {
    @Autowired
    private OrderRepo orderRepo;
    @Autowired
    private ProductRepo productRepo;
    @Autowired
    private ClientRepo clientRepo;
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private ApplicationContext applicationContext;
    private BlockingQueue<Order> ordersQueue = new LinkedBlockingDeque<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    @Async
    public void addOrderToQueue(Order order) {
        LOGGER.info("Adding new order to queue " + order);
        ordersQueue.add(order);
    }

    @Async
    public void addOrdersToQueue(List<Order> orders) {
        LOGGER.info("Adding new orders to queue " + orders);
        ordersQueue.addAll(orders);
    }

    @Lookup
    public OrderProcessingTask createOrderProcessingTask() {
        return null;
    }

    @Lookup
    public RecoveryTask createOrderRecoveryTask() {
        return null;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void initOrderServiceThreads() {
        LOGGER.info("Starting worker threads");
        startProcessing();
        executeRecovery();
    }
    /**
     * Create recovery task in a separate thread
     * and runs processing of NEW/PENDING orders
     */
    public void executeRecovery() {
        var recoveryThread = createOrderRecoveryTask();
        taskExecutor.execute(recoveryThread);
    }

    /**
     * Create main processing task in a separate thread
     * Processes NEW/PENDING orders
     */
    public void startProcessing() {
        var orderProcessingTask = createOrderProcessingTask();
        orderProcessingTask.setProcessingQueue(ordersQueue);
        taskExecutor.execute(orderProcessingTask);
    }
}
