package co.spribe.store;

import co.spribe.store.core.model.Order;
import co.spribe.store.core.repos.ClientRepo;
import co.spribe.store.core.repos.ProductRepo;
import co.spribe.store.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.StreamSupport;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@Sql({"/store.sql"})
class StoreApplicationTests {

	@Autowired
	private ProductRepo productRepo;
	@Autowired
	private ClientRepo clientRepo;
	@Autowired
	private OrderService orderService;

	@Test
	public void testLoadDataForProducts() {
		Assertions.assertEquals(10, StreamSupport.stream(productRepo.findAll().spliterator(), false).count());
	}

	@Test
	public void testLoadDataForClients() {
		Assertions.assertEquals(2, StreamSupport.stream(clientRepo.findAll().spliterator(), false).count());
	}

	@Test
	public void testWithTwoCustomerOrdersAtOneTime() throws BrokenBarrierException, InterruptedException {
		final CyclicBarrier gate = new CyclicBarrier(3);
		Thread t1 = new Thread(() -> {
			try {
				gate.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
			Map<Long, Integer> productsMap = new HashMap<>();
			productsMap.put(1L, 1);
			Order order = new Order(productsMap, 1);
			orderService.addOrderToQueue(order);
		});
		Thread t2 = new Thread(() -> {
			try {
				gate.await();
			} catch (InterruptedException | BrokenBarrierException e) {
				e.printStackTrace();
			}
			Map<Long, Integer> productsMap = new HashMap<>();
			productsMap.put(2L, 3);
			Order order = new Order(productsMap, 2);
			orderService.addOrderToQueue(order);
		});
		t1.start();
		t2.start();

		gate.await();

		//Make sure product amount decreased
		Thread.sleep(5000);
		Assertions.assertEquals(9, productRepo.findById(1L).get().getAmount());
		Assertions.assertEquals(7, productRepo.findById(2L).get().getAmount());
	}
}
