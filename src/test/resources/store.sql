LOCK TABLES `clients` WRITE;
DELETE FROM `clients`;
INSERT INTO `clients` VALUES (1,0,'John','Doe'),(2,0,'Alice','Cooper');
UNLOCK TABLES;

LOCK TABLES `products` WRITE;
DELETE FROM `products`;
INSERT INTO `products` VALUES (1,0,10,'Apple Laptop',1500,3),(2,0,10,'Cool sport car',100000,1500),(3,0,10,'Chair',100,8),(4,0,10,'Toy gun',20,1),(5,0,10,'Book Java for Dummies',20,0.5),(6,0,10,'Backpack',40,1),(7,0,10,'Watch',1100,0.4),(8,0,10,'Smartphone',500,0.2),(9,0,10,'Shoes',100,1.6),(10,0,10,'Earphones',150,0.4);
UNLOCK TABLES;
