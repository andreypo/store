FROM openjdk:14-jdk-alpine
MAINTAINER store_multithreading@gmail.com
COPY target/store-0.0.1-SNAPSHOT.jar store-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/store-0.0.1-SNAPSHOT.jar"]
